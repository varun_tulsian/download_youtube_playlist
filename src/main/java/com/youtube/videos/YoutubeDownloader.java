package com.youtube.videos;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

public class YoutubeDownloader {
    public static Map<String,String> configMap;
    public static Map<String,String> downloadConfigMap;
    private final static ObjectMapper objectMapper = new ObjectMapper();
    private final static String DOWNLOAD_LOCATION;
    static{
        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.INFO);
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
        try {
            configMap = objectMapper.readValue(new FileInputStream("src/main/resources/config.json"), new TypeReference<Map<String,String>>() {});
            downloadConfigMap = objectMapper.readValue(new FileInputStream("src/main/resources/download_config.json"), new TypeReference<Map<String,String>>() {});
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        DOWNLOAD_LOCATION = downloadConfigMap.get("download_location");
        File file = new File(DOWNLOAD_LOCATION);
        try {
            Files.createDirectories(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static int MAX_VIDEOS = 80;

    public static void main(String[] args) throws IOException, InterruptedException {
        start();
    }

    public static void start() throws IOException, InterruptedException {
        WebClient webClient = new WebClient(BrowserVersion.CHROME);
        setOptions(webClient);
        HtmlPage youtubePlaylistPage = null;
        try {
            youtubePlaylistPage = webClient.getPage(downloadConfigMap.get("youtube_url"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert youtubePlaylistPage != null;

        final List<HtmlAnchor> anchors1 = (List<HtmlAnchor>) youtubePlaylistPage.getByXPath(configMap.get("video_url_xpath"));
        final List<HtmlElement> titles1 = (List<HtmlElement>) youtubePlaylistPage.getByXPath(configMap.get("title_xpath"));

        while(true){
            final List<HtmlElement> titles = new ArrayList<HtmlElement>();
            final List<HtmlAnchor> anchors = new ArrayList<HtmlAnchor>();

            for (int i=0; i<titles1.size() && i< MAX_VIDEOS; i++){
                HtmlElement title = titles1.get(i);
                if (!isDownloaded(title)){
                    System.out.println("Got the title "+ title.getTextContent());
                    titles.add(title);
                    anchors.add(anchors1.get(i));
                } else{
                    System.err.println("this song is already downloaded "+ title.getTextContent());
                }
            }

            if (anchors.size()==0){
                break;
            }

            for (int i=0; i<anchors.size() && i<MAX_VIDEOS; i++){
                ///////get download page
                HtmlPage downloadVideosPage = null;
                try {
                    downloadVideosPage = webClient.getPage(configMap.get("download_url"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                assert downloadVideosPage != null;

                HtmlTextInput textbox = ((List<HtmlTextInput>) downloadVideosPage.getByXPath(configMap.get("textbox_xpath"))).get(0);
                HtmlSubmitInput button = ((List<HtmlSubmitInput>) downloadVideosPage.getByXPath(configMap.get("button_xpath"))).get(0);

                ////////////

                HtmlAnchor anchor = anchors.get(i);
                String title = titles.get(i).getTextContent().replace("\\","\\\\");
                textbox.setAttribute("value", "http://www.youtube.com" + anchor.getHrefAttribute());
                HtmlPage downloadPage = button.click();
                HtmlAnchor dlLink = null;
                List<HtmlAnchor> dlLinks = null;
                int ii=0;
                while (true){
                    Thread.sleep(5000);
                    if (ii==5){
                        System.out.println(":( skipping --> "+title);
                        break;
                    }
                    ii++;
                    dlLinks = ((List<HtmlAnchor>)downloadPage.getByXPath(configMap.get("download_xpath")));
                    if (dlLinks!= null && dlLinks.size()>0){
                        break;
                    }
                }
                if (dlLinks == null){
                    System.err.println("couldn't not get download link :(");
                    continue;
                }
                for (HtmlAnchor anchor1 : dlLinks){
                    if (anchor1.getHrefAttribute().contains("ts_create")){
                        dlLink = anchor1;
                        break;
                    }
                }
                if (dlLink == null){
                    System.err.println("couldn't not get download link :(");
                    continue;
                }
                System.out.println("download link ---> " +dlLink.getHrefAttribute());
                File f = new File(DOWNLOAD_LOCATION +title+".mp3");
                InputStream inputStream = dlLink.click().getWebResponse().getContentAsStream();
                Files.deleteIfExists(f.toPath());
                Files.copy(inputStream, f.toPath());
                inputStream.close();
                System.out.println("downloaded " + title);
            }
        }
    }

    private static boolean isDownloaded(HtmlElement title) {
        File f = new File(DOWNLOAD_LOCATION +title.getTextContent().replace("\\","\\\\")+".mp3");
        return f.exists();
    }

    private static void setOptions(WebClient webClient) {
        webClient.getOptions().setJavaScriptEnabled(true);
        webClient.getOptions().setCssEnabled(false);
        webClient.getOptions().setUseInsecureSSL(true);
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        webClient.getCookieManager().setCookiesEnabled(true);
        webClient.setAjaxController(new NicelyResynchronizingAjaxController());
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.getCookieManager().setCookiesEnabled(true);
    }
}
