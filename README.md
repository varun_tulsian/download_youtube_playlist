Downloads songs from a youtube playlist. Sample playlist --- https://www.youtube.com/watch?v=c1oPZBVRTm4&list=LL5fwedajKdGMt05Eb9XlscA. 

Uses http://www.youtube-mp3.org/ to download mp3 versions of the song. Make config changes to download_config.json. 